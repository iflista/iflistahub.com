---
layout: post
title: "So the real purpose of blogging.."
date: 2013-04-18 22:07
comments: true
categories: 
---
It speaks for itself. I have sloppy english. I didn't learn it in school and in a university neither. I make a lot of mistakes. And blogging in English is almost perfect tool to improve myself in that area. Also I hope it helps me better define my goals and achieve them.